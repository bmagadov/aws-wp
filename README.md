# AWS CloudFormation Deploying WordPress Infrastructure
## with autoscaling, loadbalancing, external Database with replication and configuring for using CloudFront


**VPC with multi-AZ subnets, Security Groups, RDS with replication, S3 bucket, IAM user, AutoScaling Group & Load Balancing with Scale Up & Scale Down policies**

!!! Before creating CF stack you have to create keypair for EC2 instances !!!  
To enable database replication you have to uncomment lines in "Database Replication" step.  
This CF stack is for N.Virginia region. To deploy in another region you have to set AMI parameter specific for region.  
S3 bucket name must be unique. Replace "wps3bucket.clfr-1919" with unique new S3 bucket name.  

You can specify VPC IP addresses, database credentials by your own.  
Don't forget set correct keypair name before creating CF stack.  


#### At the "Review MyStack" in "Capabilities" check "I acknowledge that AWS CloudFormation might create IAM resources with custom names


#### After CF stack deployment connect WordPress to S3 bucket by following next steps:

1) Login to WP admin pannel  
2) Click to the Plugins - Install new  
3) Search "Offload Media Lite" plugin, install it and activate  
4) Write your S3 bucket name click next and save configuration  

---

### VPC with multi-AZ subnets

Creates VPC with 2 availability zones, 2 public subnets and 2 private subnets for databases

---

### Security Groups

#### Public security group
Allows public access to the following ports: 22, 80, 443

#### Database security group
Allows private access to 3306 port

---

### RDS with replication
Creates Multi-AZ MySQL Database with replication

---

### S3 bucket

Creates S3 bucket with public read access

---

### IAM user

Creates IAM user with programmatic access and policy to allow modifying access for S3 bucket

---

### AutoScaling Group & Load Balancing with Scale Up & Scale Down policies

Creates AutoScaling group from 1 to 6 EC2 instances scale range in 2 availability zones.  
When the CPU load is more than 90 percent, a clone of the virtual machine is created and the load is balanced between them. If the load drops to 40 percent, the deletion of the virtual machine is initiated.  
UserData initiate connection to the private git repository and triggers ansible playbook, that installs docker and wordpress container, connects it to RDS and configures wp-config.php parameters for getting permissions to add and edit files in S3 bucket